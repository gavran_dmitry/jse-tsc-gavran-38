package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.gavran.tm.api.service.IConnectionService;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.service.ConnectionService;
import ru.tsc.gavran.tm.service.PropertyService;
import ru.tsc.gavran.tm.service.SessionService;
import ru.tsc.gavran.tm.service.UserService;

import java.util.List;

public class UserRepositoryTest {

    @Nullable
    private UserRepository userRepository;

    @Nullable
    private UserService userService;

    @Nullable
    private User user;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private Session session;

    @NotNull
    protected static final String TEST_USER_LOGIN = "Admin25";

    @NotNull
    protected static final String TEST_USER_EMAIL = "Admin225yandex.ru";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "Admin25";

    @Before
    public void before() {
        IConnectionService connectionService = new ConnectionService(new PropertyService());
        userRepository = new UserRepository(connectionService.getConnection());
        userService = new UserService(connectionService, bootstrap.getPropertyService());
        userService.create(TEST_USER_LOGIN,TEST_USER_PASSWORD);
        this.user = userService.findByLogin(TEST_USER_LOGIN);
        user.setEmail(TEST_USER_EMAIL);
    }

    @After
    public void after() {
        userService.removeByLogin(TEST_USER_LOGIN);
    }

    @Test
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(TEST_USER_LOGIN, user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(TEST_USER_EMAIL, user.getEmail());
        @Nullable final User userById = userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    public void findAll() {
        @Nullable final List<User> users = userRepository.findAll();
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    public void findById() {
        @Nullable final User user = userRepository.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = userRepository.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

}