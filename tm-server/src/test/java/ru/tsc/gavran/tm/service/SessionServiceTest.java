package ru.tsc.gavran.tm.service;

import org.checkerframework.checker.units.qual.C;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.repository.SessionRepository;

import java.util.List;

public class SessionServiceTest {

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @Nullable
    private ConnectionService connectionService;

    @Nullable
    private User user = new User();;

    @NotNull
    protected static final String TEST_USER_LOGIN = "Admin22";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "Admin22";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin", "Admin");
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        @NotNull final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionService.findAll();
        Assert.assertNotNull(session);
        Assert.assertTrue(session.size() > 0);
    }

    @Test
    public void open() {
        sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
    }

    @Test
    public void validate() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        sessionService.validate(session);
        Assert.assertNotNull(session);
    }

}