package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.Task;


import java.util.List;

public class TaskServiceTest {

    @Nullable
    private static TaskService taskService;

    @Nullable
    private static Task task;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_TASK_NAME = "TaskTestName";

    @NotNull
    protected static final String TEST_DESCRIPTION = "TaskTestDescription";

    @NotNull
    protected static final String TASK_TEST_USER_ID = "TaskTestUserId";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin11", "Admin11");
        taskService = new TaskService(connectionService);
        task = taskService.add(session.getUserId(), new Task(TEST_TASK_NAME, TEST_DESCRIPTION));
    }

    @Test
    public void findById() {
        @NotNull final Task tasks = taskService.findById(task.getId());
        Assert.assertEquals(TEST_TASK_NAME, tasks.getName());

    }
    @After
    public void after() {
        taskService.remove(session.getUserId(), task);
        sessionService.close(session);
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void create() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());

        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }


    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskService.changeStatusById(session.getUserId(), this.task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskService.changeStatusByName(session.getUserId(), TEST_TASK_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    };

    @Test
    public void changeStatusByIndex() {
        @Nullable final Task task = taskService.changeStatusByIndex(session.getUserId(), 1, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    };

    @Test
    public void findByName() {
        @Nullable final Task task = taskService.findByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
    };

    @Test
    public void removeByName() {
        taskService.removeByName(TASK_TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
    };

    @Test
    public void updateByIndex() {
        @Nullable final Task task = taskService.updateByIndex(session.getUserId(),1, TEST_TASK_NAME, TEST_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getDescription(), TEST_DESCRIPTION);
    };

    @Test
    public void updateById() {
        @Nullable final Task task = taskService.updateById(session.getUserId(),this.task.getId(), TEST_TASK_NAME, TEST_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getDescription(), TEST_DESCRIPTION);
    };

    @Test
    public void startById() {
        @Nullable final Task task = taskService.startById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    };

    @Test
    public void startByName() {
        @Nullable final Task task = taskService.startByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    };

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskService.startByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    };

    @Test
    public void finishById() {
        @Nullable final Task task = taskService.finishById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    };

    @Test
    public void finishByName() {
        @Nullable final Task task = taskService.finishByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    };

    @Test
    public void finishByIndex() {
        @Nullable final Task task = taskService.finishByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    };

}
