package ru.tsc.gavran.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Nullable
    @Getter
    @Setter
    protected String userId;

}